#!/bin/bash

echo 'please modify the $names variable to be your own id'
declare -a names=("abc") #modify to be your own id
timeo=200s

function _SC() #safe call
{
 # echo "[RUNNING] : $*"
  eval $*
  if [ $? -ne 0 ]; then
    echo "[ERROR] : command $* failed, please check, exiting..."
    exit
  fi
}

function getNumber()
{
	#assuming already in bin directory  
	local cfile=$datapwd/$2/$1
	local input=$datapwd/$2/$2-InputSet/${1%.c}.in
	local answer=$datapwd/$2/$2-AnswerSet/${1%.c}.ans
	#echo $cfile
	if [[ ! -f $answer ]]; then
	  echo "[ERROR] : $filea does not exist, exiting..."
	  exit
	fi


	_SC cp $cfile data.c
	echo "now $tcase" >> $resdir/$name.LOG
	(timeout $timeo $CCHK < data.c > ${1%.c}.s) 1>LOGNOW 2>&1
	cat LOGNOW | tee -a $resdir/$name.LOG
	if [ $? -ne 0 ]; then
		NN=$wrongN
		echo "compile failed(timeout or ce)"
		return 1
	fi
	if [ -f $input ]; then
		(timeout $timeo spim -stat_file spimstat -file ${1%.c}.s) < $input 1>${1%.c}.out 2>spimerr
	else
		(timeout $timeo spim -stat_file spimstat -file ${1%.c}.s) 1>${1%.c}.out 2>spimerr
	fi
	if [ $? -ne 0 ]; then
		NN=$wrongN
		echo "spim running failed"
		return 1
	fi
 	diff ${1%.c}.out $answer >/dev/null 2>&1
	if [ $? -ne 0 ]; then
		NN=$wrongN
		echo "incorrect:	$1"
		return 1
	fi
	NN=$(tail spimstat | awk ' { printf $1 } ')
	echo "$NN"
#	:	${1%-*-*}"
}

outdir=$(pwd)

datapwd=$(pwd)/compiler2015-testcases/Phase5/
inh=$datapwd/Inherited
new=$datapwd/New

resdir=$(pwd)/results
scdir=$(pwd)/scores

for name in ${names[@]}; do
  echo now testing ${name}...
  _SC cd students
  _SC cd $name/compiler2015
  
  if [ ! -e ./finalvars.sh ]; then 
    echo "[ERROR] : can't find finalvars.sh, exiting..." 
    exit 
  fi 

  unset -v CCHK 
  #cat midtermvars.sh | sed 's/^/_SC /g' | sed 's/"/\\"/g' | sed 
"s/'/\\\\'/g" >/tmp/finalvars.sh #not good solution 
  set -x 
  source ./finalvars.sh #can't ensure SC here 
  set +x 
  echo CCHK=$CCHK 

  rm $resdir/$name.LOG 
  rm $scdir/${name}.score
  wrong=0
  _SC cd bin 

  for tcase in $(ls $inh/*.c); do
	getNumber $(basename $tcase) $(basename $inh)
	#echo Number is $NN
	printf "%d\n" $NN >> $scdir/${name}.score
  done
  for tcase in $(ls $new/*.c); do
	getNumber $(basename $tcase) $(basename $new)
	#echo Number is $NN
	printf "%d\n" $NN >> $scdir/${name}.score
  done
  echo "LOG saved to $resdir/$name.LOG" 

  _SC cd .. 
  cd $outdir 
done 
