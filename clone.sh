#!/bin/bash

echo 'please modify the $names variable to be your own id'
declare -a names=("abc") #modify to be your own id 
timeo=10s

function _SC() #safe call
{
  echo "[RUNNING] : $*"
  eval $*
  if [ $? -ne 0 ]; then
    echo "[ERROR] : command $* failed, please check, exiting..."
    exit
  fi
}

outdir=$(pwd)

mkdir -p students

if [ ! -d compiler2015-testcases ]; then
  _SC git clone https://acmcompiler@bitbucket.org/acmcompiler/compiler2015-testcases.git

else
  _SC cd compiler2015-testcases
  _SC git pull
  _SC cd ..
fi

mkdir -p results
resdir=$(pwd)/results

for name in ${names[@]}; do
  echo now testing ${name}...
  _SC cd students
  if [ ! -d $name ]; then
    _SC mkdir $name "&&" cd $name
    _SC git clone "https://acmcompiler@bitbucket.org/${name}/compiler2015.git"

    _SC cd ..
  fi
  _SC cd $name/compiler2015
  _SC git checkout master
  _SC git pull
  _SC git fetch --tags
  _SC git checkout final #pay attention to your tag! 
  _SC make clean 
  _SC make all 

  cd $outdir 
done 
